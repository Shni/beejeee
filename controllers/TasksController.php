<?php


class TasksController extends Controller
{

    public function index()
    {
        $this->view('layouts/index');
    }

    public function all($page = null)
    {
       $table = 'tasks';
       $limit = 3;
       $pager = $this->paginator->makePager($table,$limit);
       $tasks = $this->paginate($table, $limit, $page);
       $this->view('layouts/tasks', $tasks, $pager);

    }

    public function create()
    {
        $this->view('layouts/form');
    }

    public function store()
    {
        $this->model->createTask($_POST['name'], $_POST['email'], $_POST['task'], $_POST['img']);
        $this->view('layouts/index');
    }

}