<?php
$url = Handler::getUrl();

if (is_array($data)) {
    if (Session::get('admin') == true || $url == URL . 'admin/edit') {
        foreach ($data as $task) {
            $name = $task['name'];
            $email = $task['email'];
            $tasks = $task['task'];
            $img = $task['img'];
            $action = '/admin/update/' . $task['id'];
        }
    }
} else {
    $action = '/task/store';
    $name = '';
}
3
?>


<div id="form" class="row">
    <div class="col-sm-6 col-md-5">
        <h1> Додати Завдання </h1>
        <div class="thumbnail">
            <form method="post" action="<?= $action ?>">
                <div class="caption">
                    <div class="form-group"
                        <label for="name">Name</label><br>
                        <input class="form-control" type="name" name="name" value="<?= $name ?>">
                    </div>
                    <div class="form-group">
                        <label for="photo">Email</label><br>
                        <input class="form-control" type="text" name="email" value="<?= $email ?>">
                    </div>
                    <div class="form-group">
                        <label for="name">Task</label><br>
                        <textarea class="form-control" type="text" name="task"
                                  rows="5"><?= $tasks ?> </textarea>
                    </div>
                    <div class="form-group">
                        <label for="photo">Photo</label><br>
                        <input class="form-control" type="text" name="img" value="<?= $img ?>">
                    </div>

                    <div>
                        <a href="/" class="btn btn-default" role="button">Home</a>
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
