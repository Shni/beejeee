<?php


class LoginController extends Controller
{

    function index()
    {
        Session::init();
        if (Session::get('admin') == true){
            header('location:' . URL);
        } else $this->view('layouts/login');
    }

    public function verify()
    {
        $admins = $this->model->where('login', '=', $_POST['login']);

        if (isset($admins)) {
            foreach ($admins as $admin) {
                if (password_verify($_POST['password'], $admin['password']) == 1) {
                    Session::init();
                    Session::set('admin', true);
                    header('location:' . URL);
                } else {
                    header('location:' . LOGIN);
                }
            }
        } else {
            header('location:' . LOGIN);
        }
    }
}