<?php


class AdminsController extends Controller
{
    function __construct()
    {
        $this->middleware('admin');
    }

    function index()
    {
        $this->view('layouts/index');
    }

    function edit($id)
    {
        $admins = $this->model->find($id);
        $this->view('layouts/form', $admins);
    }

    function update($id)
    {
        $this->model->update([
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'task' => $_POST['task'],
            'img' => $_POST['img']
        ], $id);
        $this->view('layouts/index');
    }


    function logout()
    {
        Session::destroy();
        header('location:' . URL);
        exit;
    }
}
