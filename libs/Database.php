<?php



class Database extends PDO
{
    function __construct()
    {
        parent::__construct(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
    }

    public function execute($expr)
    {
        $sth = $this->prepare($expr);
        $sth->execute();
        return $sth;
    }

    public function fetch($expr)
    {
       return $this->execute($expr)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function fetchColumn($expr)
    {
        return $this->execute($expr)->fetchColumn();
    }
}