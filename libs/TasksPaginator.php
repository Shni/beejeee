<?php


class TasksPaginator
{
    function __construct()
    {
        $this->model = new Model();
    }


    public function pagination($table,$page, $limit)
    {

        $totalPages =  $this->model->CountRows($table) / $limit;;

        if (is_null($page)  || $page <= 0) {
            $page = 1;
        }elseif ($page > $totalPages){
            $page = $totalPages;
        }
        $offset = (($page) * $limit) - $limit;

        return $this->model->fetchForPagination($table, $limit, $offset);
    }

    public function makePager($table, $limit)
    {
        $totalPages =  $this->model->CountRows($table) / $limit;
        echo '<div class="clear"></div>';

        for ($page = 1; $page <= $totalPages; $page++) {
            echo "<div class=\"pagination\">" .
                "<li><a href=" . URL . 'task/all/' . $page . ">" . $page . "</a></li>" .
                "</div>";
        }

        echo '<div class="clear"></div>';


    }

}
