<?php


class Bootstrap
{

    function __construct()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);

        if (empty($url[0])) {
            require 'controllers/TasksController.php';
            $controller = new TasksController();
            $controller->index();
            return false;
        }

        $model = ucwords($url[0]);
        if ($url[0] == 'login'){
            $url[0] = ucwords($url[0]) . 'Controller';
        }else {
            $url[0] = ucwords($url[0]) . 'sController';
        }
        $file = 'controllers/' . $url[0] . '.php' ;




        if (file_exists($file)) {
            require $file;
        } else {
            $this->error();
        }


        $controller = new $url[0]();
        $controller->loadModel($model);

        if (isset($url[2])) {
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2]);
            } else {
                $this->error();
            }
        } else {
            if (isset($url[1])) {
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}();
                } else {
                    $this->error();
                }
            } else {
                $controller->index();
            }
        }

    }


    public function error()
    {
        require 'controllers/ErrorsController.php';
        $error = new ErrorsController();
        $error->index();
        return false;
    }


}