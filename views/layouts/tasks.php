
<div class="row">
    <?php foreach ($data as $task): ?>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="<?= $task['img'] ?>" alt="<?= $task['name'] ?>">
                <div class="caption">
                    <h3><?= $task['name'] ?></h3>
                    <p><em><?= $task['email'] ?></em></p>
                    <p><?= $task['task'] ?></p>
                    <?php if (Session::get('admin') == true) : ?>
                        <a href="<?= URL ?>admin/edit/<?= $task['id'] ?>" class="btn btn-primary">Редагувати</a>
                    <? endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<?php $paginate ?>
