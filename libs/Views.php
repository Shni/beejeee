<?php


class Views
{
    function __construct()
    {
        $this->controller = new Controller();
    }

    function render($name, $data = '', $noInclude = false )
    {

        if ($noInclude == true) {
            require 'views/' . $name . '.php';
        } else {
            $data;
            require 'views/header.php';
            require 'views/' . $name . '.php';
            require 'views/footer.php';
        }
    }
}