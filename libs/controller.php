<?php



class Controller
{
    function __construct()
    {
        $this->paginator = new TasksPaginator();
    }


    function middleware($name)
    {
        Session::init();
        $logged = Session::get($name);
        if ($logged == false) {
            Session::destroy();
            header('location:' . LOGIN );
            exit;
        }
    }

    function view($route, $include = false)
    {
        $views  = new Views();
        $views->render($route, $include);
    }

    function paginate($table, $limit, $page)
    {
        return $this->paginator->pagination($table, $page, $limit);
    }

    function loadModel($name)
    {
        $path = 'models/'. $name . '.php';

        if (file_exists($path)){
            require $path;
            $this->model = new $name();
        }
    }

}