<?php


class Model
{
    public function __construct()
    {
        $this->db = new Database();
    }

    public function CountRows($tab)
    {
        return $this->db->fetchColumn("SELECT  COUNT(*) FROM " . $tab);
    }

    public function getAll($tab)
    {
        return $this->db->fetch("SELECT * FROM " . $tab );
    }

    public function fetchWhere($tab , $column = null, $operator = null, $value = '')
    {
        return $this->db->fetch("SELECT * FROM $tab WHERE $column  $operator  '$value'");
    }

    public function fetchWhereId($tab , $id)
    {
        return $this->db->fetch("SELECT * FROM $tab WHERE id =  $id");
    }

    public function insert($table, $fields, $value )
    {
         return $this->db->execute("INSERT INTO $table ($fields) VALUES($value) ");

    }

    public function updateWhere($tab, $values, $id )
    {
        return $this->db->execute("UPDATE $tab SET $values WHERE id = $id");
    }

    public function fetchForPagination($table, $limit, $offset)
    {
        return $this->db->fetch("SELECT * FROM $table ORDER BY id DESC LIMIT $limit OFFSET $offset");

    }




}