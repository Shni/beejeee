<?php


class Admin extends Model
{

    function find($id)
    {
        return  $this->fetchWhereId('tasks',$id);
    }

    function update(array $values, $id)
    {
         return $this->updateWhere('tasks', Handler::fields($values), $id);
    }

}